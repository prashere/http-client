Trying to implement http-client in python as a library for academic understanding following [RFC2616](https://tools.ietf.org/html/rfc2616), [RFC7230](https://tools.ietf.org/html/rfc7230)

Usage:
-----

1. Invoking the client:

        from http_client import HTTPClient
        client = HTTPClient()

2. Add headers:

        client.addheader("Accept-Langugage", "en")

3. GET Request:

        client.http_request(url="http://www.fsftn.org/events/sc/2016/chn/", method="GET")
        print(client.response.decode())
