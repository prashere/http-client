#!/usr/bin/env python3

__author__ = "Prasanna Venkadesh"
__license__ = "GPL v3"
__status__ = "development"

import socket
from urllib import parse

class HTTPClient:
    
    def __init__(self, port=80):
        self.port = port
        self.header_dict = dict()
        self.headers = None
        self.request_line = None
        self.method = None
        self.message_body = ""
        self.host = ""
        self.path = ""
        self.request = ""
        self.response = ""

    def addheader(self, key, value):
        if key not in self.header_dict:
            self.header_dict[key] = value

    def http_request_message(self):
        self.request_line = self.method + " " + self.path + " HTTP/1.1" + "\r\n"

        header_list = ["User-Agent: python3-http\r\n", "Host: "+self.host+"\r\n"]
        # iterate through headers_dict and append to list
        for key, value in self.header_dict.items():
            header_list.append(key + ": " + value + "\r\n")

        # convert the list into a string
        self.headers = "".join(header_list)

        if self.method == "GET":
            self.request = self.request_line + self.headers + "\r\n"

        return self.request

    def http_request(self, url, method):
        parse_result = parse.urlparse(url)

        self.host = parse_result.hostname
        self.path = parse_result.path
        self.method = method

        tcp_client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_client_socket.connect((self.host, self.port))

        self.http_request_message()
        tcp_client_socket.send(self.request.encode())

        self.response = tcp_client_socket.recv(4096)
